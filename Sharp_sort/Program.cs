﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Sharp_sort
{
    class Program
    {
        public static int Max(int a)
        {
            var digList = new List<int>();
            while (a > 0)
            {
                digList.Add(a % 10);
                a /= 10;
            }
            int count = 0;
            int max = 0;
            for (int j = 1; j < digList.Count; j++)
            {
                if (digList[j] == digList[j - 1])
                    count++;
                else
                    count = 0;
                if (count > max)
                    max = count;
            }

            return max;
        }
        static void Main(string[] args)
        {
            StreamReader sr;
            try
            {
                sr = new StreamReader("sort.in");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Ошибка открытия файла:\n" + ex.Message);
                return;
            }
            try
            {
                if (sr.Peek() == -1)
                { 
                    throw new IOException();
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Файл пуст!" + ex.Message);
                return;
            }

            StreamWriter sw = new StreamWriter("sort.out");
            string s;
            var numList = new List<int>();
            while (!sr.EndOfStream)
            {
                s = sr.ReadLine();
                numList.Add(Int32.Parse(s));
            }

            IEnumerable<int> sorted = numList.OrderByDescending(n => Max(n));

            foreach (int i in sorted)
                sw.WriteLine(i);

            sr.Close();
            sw.Close();
        }
    }
}